package com.aves.AvesMongoRest.models;

import org.springframework.data.mongodb.core.mapping.Document;


public class skill {

    String title;
    String rating;

    public skill() {
    }

    public skill(String title, String rating) {
        this.title = title;
        this.rating = rating;
    }
 
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

}
