package com.aves.AvesMongoRest.controllers;

import com.aves.AvesMongoRest.models.projectMembers;
import com.aves.AvesMongoRest.repositories.ProjectMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProjectMemberController {

    @Autowired
    ProjectMemberRepository productRepository;

    @RequestMapping(method=RequestMethod.GET, value="/api/members")
    public Iterable<projectMembers> projectMembers() {
    	System.out.println("#################################### calling the service");
        return productRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/api/members")
    public String save(@RequestBody projectMembers projectMembers) {
        productRepository.save(projectMembers);

        return projectMembers.get_Id();
    }

    @RequestMapping(method=RequestMethod.GET, value="/api/members/{id}")
    public projectMembers show(@PathVariable String id) {
    System.out.println("####################### : " + id );
    	
    	return productRepository.findById(id).get();
    }

    @RequestMapping(method=RequestMethod.PUT, value="/api/members/{id}")
    public projectMembers update(@PathVariable String id, @RequestBody projectMembers projectMembers) {
        projectMembers prod = productRepository.findById(id).get();
        
        if(projectMembers.getName() != null)
            prod.setName(projectMembers.getName());
        if(projectMembers.getEmail() != null)
            prod.setEmail(projectMembers.getEmail());
        if(projectMembers.getSrc_id() != null)
            prod.setSrc_id(projectMembers.getSrc_id());
        if(projectMembers.getSkills() != null)
            prod.setSkills(projectMembers.getSkills());
        if(projectMembers.getPhoto() != null)
            prod.setPhoto(projectMembers.getPhoto());
         if(projectMembers.getSrc_id() != null)
            prod.setSrc_id(projectMembers.getSrc_id());       
         
        productRepository.save(prod);
        return prod;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/api/members/{id}")
    public String delete(@PathVariable String id) {
        projectMembers projectMembers = productRepository.findById(id).get();
        productRepository.delete(projectMembers);

        return "projectMembers deleted";
    }
}
